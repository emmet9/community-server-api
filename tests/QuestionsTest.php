<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;

use Tymon\JWTAuth\JWTManager as JWT;

class QuestionsTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');

    }

    /**
     * A basic functional questions creation and listing.
     *
     * @return void
     */
    public function testBasicQuestionCreationAndList()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        //Check adding answers to a question without tags and verify proper response is received
        $response = $this->call('POST', '/api/questions', [
            'text' => 'this is the question',
            'description' => 'this is the description',
            'user' => 'user',
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());


        //Check creating a question with tags and verify proper response is received
        //creation of a question
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            'user' => 'user',
        ));

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //list the question previously created
        $response = $this->call('GET', '/api/questions/' . $responseArr['content']['id']);

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertTrue(isset($responseArr['content']['tags']));
        $this->assertTrue(count($responseArr['content']['tags']) === 3);

        foreach ($responseArr['content']['tags'] as $tag) {
            $this->assertTrue(property_exists($tag, 'id'));
            $this->assertTrue(property_exists($tag, 'name'));
        }

        //Check creating a question without tags and verify proper response is received
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',
            'user' => 'user',
        ));

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
    }


    /**
     * Check scenarios for question with answers and tags
     */
    public function testAnswersCreationAndList()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        //Question creation call
        $response = $this->call('POST', '/api/questions', array(
                'text' => 'this is the question',
                'description' => 'this is the description',
                'user' => 'user',
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //try to list a non existent answer from an existing question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers/1111');
        $this->assertEquals(404, $response->getStatusCode());

        //adding the first answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $questionId1 = $responseArr['content']['id'];

        //adding 2 answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the #tag1 answer #tag2 #tag3'
            ));
        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //List all answers for one question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers');

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the answer');
        $this->assertTrue(isset($responseArr['content'][0]->created_at));
        $this->assertTrue(isset($responseArr['content'][0]->updated_at));

        $this->assertEquals(count($responseArr['content'][1]->tags), 3);
        $this->assertTrue($responseArr['content'][1]->text === 'this is the #tag1 answer #tag2 #tag3');
        $this->assertTrue($responseArr['content'][1]->tags[0]->name === 'tag1');
        $this->assertTrue($responseArr['content'][1]->tags[1]->name === 'tag2');
        $this->assertTrue($responseArr['content'][1]->tags[2]->name === 'tag3');
        $this->assertTrue(isset($responseArr['content'][1]->created_at));
        $this->assertTrue(isset($responseArr['content'][1]->updated_at));

        //list one answer given by Id and Answer Id
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers/' . $questionId1);

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['text'] === 'this is the answer');
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertEquals(200, $response->getStatusCode());

        //try to add an answer to a question that does not exist
        $response = $this->call('POST', '/api/questions/0/answers'
            , array(
                'text' => 'this is the #tag1 answer #tag2 #tag3'
            ));
        $this->assertEquals(400, $response->getStatusCode());
    }
}