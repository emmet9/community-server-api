<?php

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'answers';

    public $incrementing = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'answers_tags');
    }
}
