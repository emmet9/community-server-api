<?php

use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
    'middleware' => ['jwt.auth'],
    'namespace' => 'Questions'
    ],
    function () {
    Route::get('/api/questions/{question_id}', ['uses' => 'RestQuestionsController@getQuestion']);
    Route::get('/api/questions', ['uses' => 'RestQuestionsController@getQuestions']);
    Route::post('/api/questions', ['uses' => 'RestQuestionsController@createQuestion']);
});

Route::group([
    'middleware' => ['jwt.auth'],
    'namespace' => 'Answers'
    ],
    function () {
    Route::get('/api/questions/{question_id}/answers', ['uses' => 'RestAnswersController@getAnswers']);
    Route::get('/api/questions/{question_id}/answers/{answer_id}', 'RestAnswersController@getAnswer');
    Route::post('/api/questions/{question_id}/answers', 'RestAnswersController@createAnswer');
});

Route::group([
    'middleware' => ['jwt.auth'],
    'namespace' => 'Tags'
    ],
    function () {
    Route::get('/api/tags', 'RestTagsController@getSuggestedTags');
});

Route::any('{all}', function () {
    return response('Not found', '404')
        ->header('Content-Type', 'text');
});