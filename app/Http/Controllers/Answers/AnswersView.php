<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Answers;

use App\Http\Controllers\Tags\TagsView;


class AnswersView
{
    public $id;
    public $user_id;
    public $text;
    public $tags;
    public $created_at;
    public $updated_at;

    /**
     * AnswersView constructor.
     * @param $answerArr
     */
    public function __construct($answerArr)
    {
        $this->id = $answerArr['id'];
        $this->user_id = $answerArr['user_id'];
        $this->text = $answerArr['text'];
        $this->created_at = strtotime($answerArr['created_at']);
        $this->updated_at = strtotime($answerArr['updated_at']);

        foreach ($answerArr['objTags'] as $tag) {
            $tagArr = $tag->toArray();
            $this->tags[] = new TagsView($tagArr);
        }
    }

}