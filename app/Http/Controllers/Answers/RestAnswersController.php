<?php

namespace App\Http\Controllers\Answers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Answer;
use Amazing\Model\Question;
use Amazing\Model\Tag;
use App\Http\Controllers\Tags;
use Illuminate\Support\Facades\Log;


class RestAnswersController extends BaseController
{

    /**
     * Get a single answer identified by answer id and question id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getAnswer(Request $request)
    {
        if (is_null($request->route('answer_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request answer started " . $request->route('question_id') . ' -- ' . $request->route('answer_id'));
        $answer = Answer::where('id', $request->route('answer_id'))
            ->where('question_id', $request->route('question_id'))
            ->first();

        if (empty($answer)) {
            return response()->make('', 404);
        }

        $answerArr = $answer->toArray();
        $answerArr['objTags'] = $answer->Tags;

        $answerView = new AnswersView(
            $answerArr
        );

        return response()->json([
                'content' => $answerView,
                'server_date' => strtotime('now')
            ]
        );
    }

    /**
     * Create an answer for a given question.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createAnswer(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request answer creation started " . $request->route('question_id'));

        $question = Question::where('id', $request->route('question_id'))->first();
        if (empty($question)) {
            return response()->make('Question does not exist', 400);
        }

        $text = $request->get('text');

        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text, $tagsRegArr);

        //Extract Tags out of the given text (and create the tags if they do not exists in db) +
        //attach them to the newly created answer
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsArr = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArr)->get();
            $tagsArr = array_flip($tagsArr);
        }

        $answer = new Answer();
        $answer->text = $text;
        $answer->question()->associate($question);
        $answer->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $answer->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $answer->tags()->saveMany($tagsToPersist);
            }
        }

        return response()->make(
            ['content' => ['id' => $answer->id]],
            201
        );
    }

    /**
     * Return all answers for a given question.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getAnswers(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request all answers started " . $request->route('question_id'));
        $question = Question::where('id', $request->route('question_id'))->first();

        if (empty($question)) {
            return response()->json('Validation error question not found', 400);
        }

        $response = [];
        foreach ($question->answers as $answers) {
            $answersArr = $answers->toArray();
            $answersArr['objTags'] = $answers->Tags;

            $response[] = new AnswersView(
                $answersArr
            );
        }

        return response()->json(
            [
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }
}
