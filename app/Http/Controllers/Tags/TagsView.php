<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Tags;


class TagsView
{
    public $id;
    public $name;

    /**
     * TagsView constructor.
     * @param $tagArr
     */
    public function __construct($tagArr)
    {
        $this->id = $tagArr['id'];
        $this->name = $tagArr['name'];
    }
}