<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Tags\TagsView;


class QuestionsView
{
    public $id;
    public $user_id;
    public $text;
    public $detail;
    public $tags;
    public $created_at;
    public $updated_at;

    /**
     * QuestionsView constructor.
     * @param $questionArr
     */
    public function __construct($questionArr)
    {
        $this->id = $questionArr['id'];
        $this->user_id = $questionArr['user_id'];
        $this->text = $questionArr['text'];
        $this->detail = $questionArr['detail'];
        $this->created_at = strtotime($questionArr['created_at']);
        $this->updated_at = strtotime($questionArr['updated_at']);

        if(isset($questionArr['objTags'])) {
            foreach ($questionArr['objTags'] as $tag) {
                $tagArr = $tag->toArray();
                $this->tags[] = new TagsView($tagArr);
            }
        }
    }

}