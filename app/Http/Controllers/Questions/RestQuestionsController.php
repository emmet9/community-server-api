<?php

namespace App\Http\Controllers\Questions;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Question;
use Amazing\Model\Tag;
use App\Http\Controllers\Tags;
use Illuminate\Support\Facades\Log;


class RestQuestionsController extends BaseController
{

    /**
     * Get a single question identified by given id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestion(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request question started " . $request->route('question_id'));
        $question = Question::where('id', $request->route('question_id'))->first();

        if (empty($question)) {
            return response()->make('', 404);
        }

        $questionArr = $question->toArray();
        $questionArr['objTags'] = $question->Tags;

        $questionsView = new QuestionsView(
            $questionArr
        );

        return response()->json([
                'content' => $questionsView,
                'server_date' => strtotime('now')
            ]
        );
    }

    /**
     * Create Question.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createQuestion(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error1', 400);
        }
        if (is_null($request->get('description'))) {
            return response()->json('Validation error2', 400);
        }

        $text = $request->get('text');
        $description = $request->get('description');
        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text . ' ' . $description, $tagsRegArr);

        //Extract Tags out of the given text and description (and create the tags if they do not exists in db) and
        //attach them to the newly created question
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsArr = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArr)->get();
            $tagsArr = array_flip($tagsArr);
        }

        $question = new Question();
        $question->text = $text;
        $question->detail = $description;
        $question->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $question->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $question->tags()->saveMany($tagsToPersist);
            }
        }

        return response()->make(
            ['content' => ['id' => $question->id]],
            201
        );
    }

    /**
     * Return all questions.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestions(Request $request)
    {
        Log::debug("Request getQuestions started " . microtime());
        $questions = Question::with('tags')->get();

        foreach ($questions as $question) {
            $questionArr = $question->toArray();
            $questionArr['objTags'] = $question->tags;

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request getQuestions ended " . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }
}
